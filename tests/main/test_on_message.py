# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import json

from paho.mqtt.client import MQTTMessage
import mock

from gating_launcher import main


@mock.patch('threading.Thread')
def test_OK(   # pylint: disable=invalid-name
    thread_mock,
    valid_userdata,
):
    thread_instance_mock = mock.MagicMock()
    thread_mock.return_value = thread_instance_mock
    msg = MQTTMessage()
    msg.payload = json.dumps({'test': 'message'})
    main.on_message(None, valid_userdata, msg)
    thread_mock.assert_called_once_with(
        target=main.conditionally_launch_gate,
        args=(
            {'test': 'message'},
            valid_userdata,
        ),
    )
    thread_instance_mock.start.assert_called_once()


def test_no_msg_JSON(   # pylint: disable=invalid-name
    valid_userdata,
    caplog,
):
    msg = MQTTMessage()
    msg.payload = 'test'
    main.on_message(None, valid_userdata, msg)
    assert "This is not a valid JSON payload" in caplog.text
