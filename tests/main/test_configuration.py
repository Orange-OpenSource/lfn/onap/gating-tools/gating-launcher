# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import configparser
import os

import mock
import pytest

from gating_launcher import main

# test of protected access method so need to disable the rule
# pylint: disable=protected-access


@mock.patch.dict(os.environ, {"CONFIG_FILE": "yolo"})
def test_get_config_no_file():
    config = main._get_config()
    assert not config.sections()


@mock.patch.dict(os.environ, {"CONFIG_FILE": "etc/gating-launcher.conf"})
def test_get_config_file():
    config = main._get_config()
    assert config.get('mqtt', 'hostname') == "localhost"


@mock.patch.dict(os.environ, {"CONFIG_FILE": "etc/gating-launcher.conf"})
def test_parse_config_ok(valid_config_parsed):
    raw_config = main._get_config()
    config = main._parse_config(raw_config)
    assert config == valid_config_parsed


@mock.patch.dict(os.environ, {"CONFIG_FILE": "yolo"})
def test_parse_config_nok():
    with pytest.raises(configparser.NoSectionError) as excinfo:
        raw_config = main._get_config()
        main._parse_config(raw_config)
    assert "No section:" in excinfo.value.message
