# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import mock


from gating_launcher import main


def test_on_connect_subscribe(caplog):
    client = mock.Mock()
    main.on_connect_subscribe(client, {'qos': 2, 'topic': 'the_topic'}, {}, 1)
    assert "connected to MQTT Broker with status 1 for subscribing" in caplog.text
    assert "subscribing on topic the_topic with qos 2" in caplog.text
    expected_calls = [mock.call.subscribe('the_topic/#', 2)]
    assert client.method_calls == expected_calls


def test_on_connect_publish(caplog):
    main.on_connect_publish(None, {}, {}, 1)
    assert "connected to MQTT Broker with status 1 for publishing" in caplog.text
