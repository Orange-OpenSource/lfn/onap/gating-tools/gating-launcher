# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import json

import mock

from gating_launcher import main


@mock.patch('gating_launcher.main.GitlabCall')
def test_newer_commit(  # pylint: disable=invalid-name
    gitlab_call_mock,
    valid_payload,
    valid_userdata,
    caplog,
):
    userdata = valid_userdata
    publish_client = mock.MagicMock()
    gitlab_call_instance_mock = mock.MagicMock(
        pipeline_id=1234,
        source_project_id=5678,
        target_project_id=9012,
        merge_request_id=3456,
        last_commit_id='abc',
        source_git_path='https://source.git',
        target_git_path='https://master.git',
        check_job=False,
    )
    userdata['publish_client'] = publish_client
    gitlab_call_mock.return_value = gitlab_call_instance_mock
    gitlab_call_instance_mock.wait_for_end.return_value = "success"
    main.conditionally_launch_gate(valid_payload, userdata)
    gitlab_call_mock.assert_called_once_with(
        'the_job',
        'abc',
        5678,
        9012,
        3456,
        'token',
        7890,
        host='https://gitlab.example.com',
    )
    publish_client.publish.assert_not_called()
    assert "A newer commit has been pushed on the merge request" in caplog.text


@mock.patch('gating_launcher.main.GitlabCall')
def test_OK(  # pylint: disable=invalid-name
    gitlab_call_mock,
    valid_payload,
    valid_userdata,
):
    userdata = valid_userdata
    publish_client = mock.MagicMock()
    gitlab_call_instance_mock = mock.MagicMock(
        pipeline_id=1234,
        source_project_id=5678,
        target_project_id=9012,
        merge_request_id=3456,
        last_commit_id='abc',
        source_git_path='https://source.git',
        target_git_path='https://master.git',

    )
    userdata['publish_client'] = publish_client
    gitlab_call_mock.return_value = gitlab_call_instance_mock
    gitlab_call_instance_mock.wait_for_end.return_value = "success"
    main.conditionally_launch_gate(valid_payload, userdata)
    gitlab_call_mock.assert_called_once_with(
        'the_job',
        'abc',
        5678,
        9012,
        3456,
        'token',
        7890,
        host='https://gitlab.example.com',
    )
    expected_payload = {
        'pipeline_id': 1234,
        'source_project_id': 5678,
        'target_project_id': 9012,
        'merge_request_id': 3456,
        'commit_sha': 'abc',
        'external_status_check_id': 7890,
        'gitlab_host': 'https://gitlab.example.com',
        'project': 'the/project',
        'source_git_path': 'https://source.git',
        'target_git_path': 'https://master.git',
        'branch': 'master',
        'change': {
            'number': '9012-3456',
            'project': 'project',
        },
        'patchSet': {
            'number': 'abc-1234',
        },
        'on_gitlab': True,
    }
    publish_client.publish.assert_called_once_with(
        'the/project/patchset-created',
        json.dumps(expected_payload),
        qos=2,
    )


@mock.patch('gating_launcher.main.GitlabCall')
def test_skipped(  # pylint: disable=invalid-name
    gitlab_call_mock,
    valid_payload,
    valid_userdata,
):
    userdata = valid_userdata
    publish_client = mock.MagicMock()
    gitlab_call_instance_mock = mock.MagicMock()
    userdata['publish_client'] = publish_client
    gitlab_call_mock.return_value = gitlab_call_instance_mock
    gitlab_call_instance_mock.wait_for_end.return_value = "skipped"
    main.conditionally_launch_gate(valid_payload, userdata)
    gitlab_call_mock.assert_called_once_with(
        'the_job',
        'abc',
        5678,
        9012,
        3456,
        'token',
        7890,
        host='https://gitlab.example.com',
    )
    publish_client.publish.assert_not_called()
    gitlab_call_instance_mock.comment.assert_not_called()


@mock.patch('gating_launcher.main.GitlabCall')
def test_timeout(  # pylint: disable=invalid-name
    gitlab_call_mock,
    valid_payload,
    valid_userdata,
):
    userdata = valid_userdata
    publish_client = mock.MagicMock()
    gitlab_call_instance_mock = mock.MagicMock()
    userdata['publish_client'] = publish_client
    gitlab_call_mock.return_value = gitlab_call_instance_mock
    gitlab_call_instance_mock.wait_for_end.return_value = "timeout"
    main.conditionally_launch_gate(valid_payload, userdata)
    gitlab_call_mock.assert_called_once_with(
        'the_job',
        'abc',
        5678,
        9012,
        3456,
        'token',
        7890,
        host='https://gitlab.example.com',
    )
    publish_client.publish.assert_not_called()
    gitlab_call_instance_mock.comment.assert_called_once_with(
        "The job the_job was either in weird state or too long {}".format(
            "to complete, no gate will be triggered",
        )
    )


@mock.patch('gating_launcher.main.GitlabCall')
def test_bad_json_message(  # pylint: disable=invalid-name
    gitlab_call_mock,
    valid_payload,
    valid_userdata,
    caplog,
):
    payload = valid_payload
    payload.pop('last_commit_id')
    main.conditionally_launch_gate(payload, valid_userdata)
    gitlab_call_mock.assert_not_called()
    assert "the key 'last_commit_id' wasn't found in the dict" in caplog.text


@mock.patch('gating_launcher.main.GitlabCall')
def test_bad_userdata(  # pylint: disable=invalid-name
    gitlab_call_mock,
    valid_payload,
    valid_userdata,
    caplog,
):
    userdata = valid_userdata
    userdata.pop('notification_subtopic')
    publish_client = mock.MagicMock()
    gitlab_call_instance_mock = mock.MagicMock(
        pipeline_id=1234,
        source_project_id=5678,
        target_project_id=9012,
        merge_request_id=3456,
        last_commit_id='abc',
        source_git_path='https://source.git',
        target_git_path='https://master.git',

    )
    userdata['publish_client'] = publish_client
    gitlab_call_mock.return_value = gitlab_call_instance_mock
    gitlab_call_instance_mock.wait_for_end.return_value = "success"
    main.conditionally_launch_gate(valid_payload, userdata)
    gitlab_call_mock.assert_called_once_with(
        'the_job',
        'abc',
        5678,
        9012,
        3456,
        'token',
        7890,
        host='https://gitlab.example.com',
    )
    publish_client.publish.assert_not_called()
    assert "the key 'notification_subtopic' wasn't found in the dict" in caplog.text
