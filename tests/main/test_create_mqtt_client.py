# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import mock


from gating_launcher import main


@mock.patch('paho.mqtt.client.Client')
def test_create_mqtt_client(client_mock, valid_config_parsed):
    config = valid_config_parsed
    config['mqtt']['transport'] = 'websockets'
    config['mqtt']['mqtt_path'] = '/'
    config['mqtt']['mqtt_headers'] = {}
    main._create_mqtt_client(  # pylint: disable=protected-access
        config,
        'client',
        main.on_connect_subscribe,
        on_message_callback=main.on_message,
        userdata={'user': 'data'},
    )
    assert client_mock.called_once()
