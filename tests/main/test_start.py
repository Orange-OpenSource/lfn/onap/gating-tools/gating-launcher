# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import os

import mock

from gating_launcher import main


@mock.patch.dict(os.environ, {"CONFIG_FILE": "etc/gating-launcher.conf", 'PRIVATE_TOKEN': 'token'})
@mock.patch('gating_launcher.main._create_mqtt_client')
def test_start(create_mqtt_client_mock, valid_userdata, valid_config_parsed):
    mqtt_client_subscribe_mock = mock.MagicMock()
    mqtt_client_publish_mock = mock.MagicMock()
    create_mqtt_client_mock.side_effect = [
        mqtt_client_publish_mock,
        mqtt_client_subscribe_mock,
    ]
    expected_userdata = valid_userdata
    expected_userdata['publish_client'] = mock.ANY
    main.start()
    expected_calls = [
        mock.call(
            valid_config_parsed,
            'mqtt.client.publish',
            main.on_connect_publish,
            userdata=expected_userdata,
        ),
        mock.call(
            valid_config_parsed,
            'mqtt.client.subscribe',
            main.on_connect_subscribe,
            on_message_callback=main.on_message,
            userdata=expected_userdata,
        ),
    ]
    create_mqtt_client_mock.assert_has_calls(expected_calls)
    mqtt_client_subscribe_mock.loop_forever.assert_called_once()
