# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import mock

from conftest import GitlabMock

from gating_launcher.gitlab_call import GitlabCall


@mock.patch('gating_launcher.gitlab_call.Gitlab')
@mock.patch.object(GitlabCall, 'get_pipeline')
def test_comment(mock_get_pipeline, mock_gitlab):
    mr_mock = mock.MagicMock()
    mock_gitlab.return_value = GitlabMock()
    gitlab_call = GitlabCall(
        "job", "abc", 1234, 5678, 9012, "token", 12, wait_time=0, merge_request=mr_mock
    )
    gitlab_call.comment('comment')
    expected_calls = [mock.call.create({'body': 'comment'})]
    assert mr_mock.notes.method_calls == expected_calls
    mock_get_pipeline.assert_called_once()
