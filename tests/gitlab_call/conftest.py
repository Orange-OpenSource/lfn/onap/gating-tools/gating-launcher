# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock

from gitlab.exceptions import GitlabGetError
import pytest

from loguru import logger

from gating_launcher.gitlab_call import GitlabCall


class JobMock():  # pylint: disable=too-few-public-methods
    def __init__(self, name, id, project_id: int = 0) -> None:  # pylint: disable=redefined-builtin,invalid-name
        self.project_id = project_id
        self.id = id  # pylint: disable=redefined-builtin,invalid-name
        self.name = name


class JobsMock:  # pylint: disable=too-few-public-methods
    def __init__(self, name, id) -> None:  # pylint: disable=redefined-builtin,invalid-name
        self.job = JobMock(name, id)

    def get(self, _id, retry_transient_errors=True):  # pylint: disable=unused-argument
        return self.job


class MergeRequestMock:  # pylint: disable=too-few-public-methods
    def __init__(self, pipe_id: int = 1234) -> None:
        self.notes = mock.Mock()
        self.head_pipeline = {"id": pipe_id}


class MergeRequestsMock:  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        self.mergerequest = MergeRequestMock(pipe_id=1234)
        self.target_mergerequest = MergeRequestMock(pipe_id=33)
        self.pipe_not_found_mergerequest = MergeRequestMock(pipe_id=75)

    def get(self, id, retry_transient_errors=True):  # pylint: disable=unused-argument,invalid-name,redefined-builtin
        if id == 74:
            return self.target_mergerequest
        if id == 13:
            return self.pipe_not_found_mergerequest
        return self.mergerequest


class PipelineMock():  # pylint: disable=too-few-public-methods
    def __init__(self, sha: str = 'abc') -> None:
        self.sha = sha


class PipelinesMock():  # pylint: disable=too-few-public-methods
    def __init__(self, fail_on_33=False) -> None:
        self.pipeline = PipelineMock()
        self.fail_on_33 = fail_on_33
        logger.debug("fail on 33: {}", self.fail_on_33)

    def get(self, id, retry_transient_errors=True):  # pylint: disable=redefined-builtin,invalid-name,unused-argument
        if self.fail_on_33 and id == 33:
            logger.debug("id = 33 and fail on 33 is set, raising an error")
            raise GitlabGetError
        if id == 75:
            raise GitlabGetError
        return self.pipeline


class ProjectMock:  # pylint: disable=too-few-public-methods
    def __init__(self, source=False) -> None:
        self.mergerequests = MergeRequestsMock()
        self.jobs = JobsMock('target job', 1)
        self.name = "target project"
        self.pipelines = PipelinesMock()
        self.http_url_to_repo = 'https://target.git'
        if source:
            logger.debug("creating a source pipeline")
            self.pipelines = PipelinesMock(fail_on_33=True)
            self.name = "source project"
            self.jobs = JobsMock('source job', 2)
            self.http_url_to_repo = 'https://source.git'


class ProjectsMock:  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        self.target_project = ProjectMock()
        self.source_project = ProjectMock(source=True)

    def get(self, id, retry_transient_errors=True):  # pylint: disable=redefined-builtin,invalid-name,unused-argument
        if id in [64, 1234]:
            logger.debug("source pipeline requested")
            return self.source_project
        logger.debug("target pipeline requested")
        return self.target_project


class GitlabMock:  # pylint: disable=too-few-public-methods
    def __init__(self) -> None:
        self.projects = ProjectsMock()


@pytest.fixture
def gitlab_call():
    with mock.patch.object(GitlabCall, 'check_freshness') as _mock_check_freshness:
        with mock.patch.object(GitlabCall, 'get_pipeline') as mock_get_pipeline:
            pipeline = PipelineMock()
            mock_get_pipeline.return_value = pipeline
            with mock.patch('gating_launcher.gitlab_call.Gitlab') as mock_gitlab:
                mock_gitlab.return_value = GitlabMock()
                return GitlabCall(
                    "two",
                    "abc",
                    1234,
                    5678,
                    9012,
                    "token",
                    12,
                    wait_time=0,
                    max_retries=5,
                )
