# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import mock

from conftest import GitlabMock, PipelineMock

from gating_launcher.gitlab_call import GitlabCall


def test_source_found(gitlab_call, caplog):
    assert isinstance(gitlab_call.get_pipeline(), PipelineMock)
    assert "Pipeline has been found on source project source project (1234)" in caplog.text
    assert "Pipeline not found in source project" not in caplog.text
    assert "Searching pipeline on target project" not in caplog.text
    assert "Pipeline not found also in target project" not in caplog.text


@mock.patch('gating_launcher.gitlab_call.Gitlab')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_target_found(_check_freshness_mock, mock_gitlab, caplog):
    mock_gitlab.return_value = GitlabMock()
    gitlab_call = GitlabCall(
        "two",
        "abc",
        1234,
        5678,
        74,
        "token",
        12,
        wait_time=0,
    )
    assert isinstance(gitlab_call.get_pipeline(), PipelineMock)
    assert "Pipeline has been found on source project target project (33)" not in caplog.text
    assert "Pipeline not found in source project" in caplog.text
    assert "Pipeline has been found on target project" in caplog.text
    assert "Pipeline not found also in target project" not in caplog.text


@mock.patch('gating_launcher.gitlab_call.Gitlab')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_target_not_found(_check_freshness_mock, mock_gitlab, caplog):
    mock_gitlab.return_value = GitlabMock()
    gitlab_call = GitlabCall(
        "two",
        "abc",
        64,
        75,
        13,
        "token",
        12,
        wait_time=0,
    )
    assert gitlab_call.get_pipeline() is None
    assert "Pipeline not found in source project" in caplog.text
    assert "Searching pipeline on target project" in caplog.text
    assert "Pipeline not found also in target project" in caplog.text
