# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
def test_source_git(gitlab_call):
    assert gitlab_call.source_git_path == 'https://source.git'


def test_target_git(gitlab_call):
    assert gitlab_call.target_git_path == 'https://target.git'
