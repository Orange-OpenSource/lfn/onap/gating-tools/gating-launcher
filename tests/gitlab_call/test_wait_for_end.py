# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import mock
from gating_launcher.constants import CANCELED_STATUS, FAILED_STATUS
from gating_launcher.constants import SKIPPED_STATUS, SUCCESS_STATUS
from gating_launcher.constants import TIMEOUT_STATUS

from gating_launcher.gitlab_call import GitlabCall


@mock.patch.object(GitlabCall, 'get_job')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_success(_check_freshness_mock, mock_get_job, gitlab_call):
    mock_job = mock.MagicMock(id=12, status=SUCCESS_STATUS)
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() == SUCCESS_STATUS
    mock_get_job.assert_called_once()


@mock.patch.object(GitlabCall, 'get_job')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_running_success(_check_freshness_mock, mock_get_job, gitlab_call):
    mock_status = mock.PropertyMock(
        side_effect=[
            "running",
            "running",
            "running",
            "success",
            "success",
            "success",
            "success",
        ],
    )
    mock_job = mock.MagicMock(id=12)
    type(mock_job).status = mock_status
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() == SUCCESS_STATUS
    assert mock_get_job.call_count == 2


@mock.patch.object(GitlabCall, 'get_job')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_failed(_check_freshness_mock, mock_get_job, gitlab_call, caplog):
    mock_job = mock.MagicMock(id=12, status=FAILED_STATUS)
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() == FAILED_STATUS
    assert "Job 12 finished with status: failed" in caplog.text
    mock_get_job.assert_called_once()


@mock.patch.object(GitlabCall, 'get_job')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_running_failed(_check_freshness_mock, mock_get_job, gitlab_call, caplog):
    mock_status = mock.PropertyMock(
        side_effect=[
            "running",
            "running",
            "running",
            "failed",
            "failed",
            "failed",
            "failed",
            "failed",
        ],
    )
    mock_job = mock.MagicMock(id=12)
    type(mock_job).status = mock_status
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() == FAILED_STATUS
    assert "Job 12 finished with status: failed" in caplog.text
    assert mock_get_job.call_count == 2


@mock.patch.object(GitlabCall, 'get_job')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_canceled(_check_freshness_mock, mock_get_job, gitlab_call, caplog):
    mock_job = mock.MagicMock(id=12, status=CANCELED_STATUS)
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() == CANCELED_STATUS
    assert "Job 12 finished with status: canceled" in caplog.text
    mock_get_job.assert_called_once()


@mock.patch.object(GitlabCall, 'get_job')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_running_canceled(_check_freshness_mock, mock_get_job, gitlab_call, caplog):
    mock_status = mock.PropertyMock(
        side_effect=[
            "running",
            "running",
            "running",
            "canceled",
            "canceled",
            "canceled",
            "skipped",
            "success",
            "success",
        ],
    )
    mock_job = mock.MagicMock(id=12)
    type(mock_job).status = mock_status
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() == CANCELED_STATUS
    assert "Job 12 finished with status: canceled" in caplog.text
    assert mock_get_job.call_count == 2


@mock.patch.object(GitlabCall, 'get_job')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_skipped(_check_freshness_mock, mock_get_job, gitlab_call, caplog):
    mock_job = mock.MagicMock(id=12, status=SKIPPED_STATUS)
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() == SKIPPED_STATUS
    assert "Job 12 finished with status: skipped" in caplog.text
    mock_get_job.assert_called_once()


@mock.patch.object(GitlabCall, 'get_job')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_running_skipped(_check_freshness_mock, mock_get_job, gitlab_call, caplog):
    mock_status = mock.PropertyMock(
        side_effect=[
            "running",
            "running",
            "running",
            "skipped",
            "skipped",
            "skipped",
            "skipped",
            "success",
            "success",
        ],
    )
    mock_job = mock.MagicMock(id=12)
    type(mock_job).status = mock_status
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() == SKIPPED_STATUS
    assert "Job 12 finished with status: skipped" in caplog.text
    assert mock_get_job.call_count == 2


@mock.patch.object(GitlabCall, 'get_job')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_timeout(_check_freshness_mock, mock_get_job, gitlab_call, caplog):
    mock_job = mock.MagicMock(id=12, status='running')
    mock_get_job.return_value = mock_job
    assert gitlab_call.wait_for_end() == TIMEOUT_STATUS
    assert "Job 12 was too long to finish" in caplog.text
    assert mock_get_job.call_count == 5


@mock.patch.object(GitlabCall, 'get_job')
@mock.patch.object(GitlabCall, 'check_freshness')
def test_no_job(_check_freshness_mock, mock_get_job, gitlab_call, caplog):
    mock_get_job.return_value = None
    assert gitlab_call.wait_for_end() == TIMEOUT_STATUS
    assert "Job Unknown was too long to finish" in caplog.text
    assert mock_get_job.call_count == 5
