# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock

from conftest import PipelineMock

from gating_launcher.gitlab_call import GitlabCall


@mock.patch.object(GitlabCall, 'get_pipeline')
def test_fresh(mock_get_pipeline, gitlab_call):
    pipeline = PipelineMock(sha='abc')
    mock_get_pipeline.return_value = 'abc'
    gitlab_call.check_freshness(pipeline)
    assert gitlab_call.check_job is True


@mock.patch.object(GitlabCall, 'get_pipeline')
def test_not_fresh(mock_get_pipeline, gitlab_call):
    pipeline = PipelineMock(sha='def')
    mock_get_pipeline.return_value = 'abc'
    gitlab_call.check_freshness(pipeline)
    assert gitlab_call.check_job is False
