# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import json

import logging
import pytest
from _pytest.logging import caplog as _caplog  # pylint: disable=unused-import
from loguru import logger
from paho.mqtt.client import MQTTMessage


@pytest.fixture
def caplog(_caplog):
    class PropogateHandler(logging.Handler):
        def emit(self, record):
            logging.getLogger(record.name).handle(record)

    handler_id = logger.add(PropogateHandler(), format="{message} {extra}")
    yield _caplog
    logger.remove(handler_id)


@pytest.fixture
def valid_payload():
    return {
        'job': 'the_job',
        'pipeline_id': 1234,
        'source_project_id': 5678,
        'target_project_id': 9012,
        'merge_request_id': 3456,
        'last_commit_id': 'abc',
        'label': 'label_1',
        'project': 'the/project',
        'branch': 'master',
    }


@pytest.fixture
def valid_msg(valid_payload):   # pylint: disable=redefined-outer-name
    payload = json.dumps(valid_payload)
    msg = MQTTMessage()
    msg.payload = payload
    return msg


@pytest.fixture()
def valid_config_mqtt_parsed():
    return {
        'qos': 2,
        'port': 1883,
        'keepalive': 60,
        'hostname': "localhost",
        'auth': {
            'username': 'username',
            'password': 'password'
        },
        'subscription_topic': 'the_topic',
        'transport': 'tcp',
        'notification_subtopic': 'patchset-created'
    }


@pytest.fixture()
def valid_config_gitlab_parsed():
    return {
        'hostname': 'https://gitlab.example.com',
        'external_status_check_id': 7890,
    }


@pytest.fixture()
def valid_config_parsed(
    valid_config_mqtt_parsed,  # pylint: disable=redefined-outer-name
    valid_config_gitlab_parsed,  # pylint: disable=redefined-outer-name
):
    return {
        'gitlab': valid_config_gitlab_parsed,
        'mqtt': valid_config_mqtt_parsed,
    }


@pytest.fixture
def valid_userdata():
    return {
        'private_token': 'token',
        'external_status_check_id': 7890,
        'gitlab_host': 'https://gitlab.example.com',
        'qos': 2,
        'topic': 'the_topic',
        'notification_subtopic': 'patchset-created',
    }
