FROM nexus3.onap.org:10001/onap/integration-python:9.1.0

LABEL name="gating_launcher" \
      version="v0.1.dev0" \
      architecture="x86_64"

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

COPY etc/gating-launcher.conf /app/etc/
COPY gating_launcher  /app/gating_launcher/

ENV PYTHONPATH="${PYTHONPATH}:/app"
ENV CONFIG_FILE=/app/etc/gating-launcher.conf

CMD [ "python3", "gating_launcher/main.py"]
