#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

from configparser import ConfigParser
from json import loads, dumps, JSONDecodeError
from os import getenv
from typing import Mapping, Union
from sys import stdout
import threading


from loguru import logger
from paho.mqtt import client as mqtt

from gating_launcher.constants import TIMEOUT_STATUS, SUCCESS_STATUS
from gating_launcher.gitlab_call import GitlabCall


def conditionally_launch_gate(json_message, userdata):
    logger.debug("conditionally_launch_gate")
    try:
        job = json_message['job']
        gitlab_call = GitlabCall(
            job,
            json_message['last_commit_id'],
            json_message['source_project_id'],
            json_message['target_project_id'],
            json_message['merge_request_id'],
            userdata['private_token'],
            userdata['external_status_check_id'],
            host=userdata['gitlab_host'],
        )
        logger_header = "project {} - project_id {} - ".format(
            json_message['project'],
            json_message['target_project_id'],
        )
        logger_header += "merge request {} - pipeline {} -".format(
            json_message['merge_request_id'],
            json_message['pipeline_id'],
        )
        logger.debug(
            "{} Waiting for end of job {}", logger_header, job)
        if not gitlab_call.check_job:
            msg = "A newer commit has been pushed {}".format(
                "on the merge request, no gate will be triggered",
            )
            logger.warning("{} {}", logger_header, msg)
            gitlab_call.comment(msg)
        else:
            job_status = gitlab_call.wait_for_end()
            if job_status == SUCCESS_STATUS:
                logger.info(
                    "{} Job {} has finished successfully, asking for a gate",
                    logger_header,
                    job,
                )
                merge_request_unique_id = "{}-{}".format(
                    gitlab_call.target_project_id,
                    gitlab_call.merge_request_id,
                )
                change_unique_id = "{}-{}".format(
                    gitlab_call.last_commit_id,
                    gitlab_call.pipeline_id,
                )
                project_name = '/'.join(
                    json_message['project'].split('/')[1:],
                )
                message_payload = {
                    'pipeline_id': gitlab_call.pipeline_id,
                    'source_project_id': gitlab_call.source_project_id,
                    'target_project_id': gitlab_call.target_project_id,
                    'merge_request_id': gitlab_call.merge_request_id,
                    'commit_sha': gitlab_call.last_commit_id,
                    'external_status_check_id': userdata['external_status_check_id'],
                    'gitlab_host': userdata['gitlab_host'],
                    'project': json_message['project'],
                    'source_git_path': gitlab_call.source_git_path,
                    'target_git_path': gitlab_call.target_git_path,
                    'branch': json_message['branch'],
                    'change': {
                        'number': merge_request_unique_id,
                        'project': project_name,
                    },
                    'patchSet': {
                        'number': change_unique_id,
                    },
                    'on_gitlab': True,
                }
                publish_client = userdata['publish_client']
                topic = "{}/{}".format(
                    json_message['project'],
                    userdata['notification_subtopic'],
                )
                logger.debug(
                    "{} Will send the following message to topic {}",
                    logger_header,
                    topic,
                )
                logger.debug(message_payload)
                publish_client.publish(
                    topic, dumps(message_payload),
                    qos=userdata["qos"]
                )
            else:
                logger.debug(
                    "{} Job {} has not finished successfully, status: {}, {}",
                    logger_header,
                    job,
                    job_status,
                    "not asking for a gate",
                )
                if job_status == TIMEOUT_STATUS:
                    msg = "The job {} was either in weird state or {}".format(
                        job,
                        "too long to complete, no gate will be triggered",
                    )
                    logger.warning("{} {}", logger_header, msg)
                    gitlab_call.comment(msg)
    except KeyError as exc:
        logger.error("the key {} wasn't found in the dict", str(exc))


def start():
    logger.remove()
    logger.add(stdout, level='DEBUG')
    config = _parse_config(_get_config())

    logger.debug("python gitlab configuration")
    userdata = {}
    userdata['private_token'] = getenv('PRIVATE_TOKEN')
    userdata['gitlab_host'] = config['gitlab']['hostname']
    userdata['external_status_check_id'] = config['gitlab']['external_status_check_id']
    userdata['qos'] = config['mqtt']['qos']
    userdata['topic'] = config['mqtt']['subscription_topic']
    userdata['notification_subtopic'] = config['mqtt']['notification_subtopic']
    logger.debug("MQTT publish client creation")
    publish_client = _create_mqtt_client(
        config,
        'mqtt.client.publish',
        on_connect_publish,
        userdata=userdata
    )
    publish_client.loop_start()
    userdata['publish_client'] = publish_client
    logger.debug("MQTT subscribe client creation")
    subscribe_client = _create_mqtt_client(
        config,
        'mqtt.client.subscribe',
        on_connect_subscribe,
        on_message_callback=on_message,
        userdata=userdata
    )
    subscribe_client.loop_forever()


def on_connect_publish(_client, _userdata, _flags, return_code):
    """Use when publish client is connected.

    Function launched when publish client is connected. Nothing is done
    except some logging.

    :param _client: the client that has been connected
    :param _userdata: the userdata defined on the client side
    :param _flags: the flags received
    :param return_code: return code of the connection

    :seealso: http://www.eclipse.org/paho/clients/python/docs/#callbacks
    """
    logger.info(
        'connected to MQTT Broker with status {} for publishing',
        return_code,
    )


def on_connect_subscribe(client, userdata, _flags, return_code):
    """Use when subscribe client is connected.

    Function launched when subscribe client is connected.
    After connection, subscribe to the parent topic is done.

    :param _client: the client that has been connected
    :param _userdata: the userdata defined on the client side
    :param _flags: the flags received
    :param rc: return code of the connection

    :type _client: paho.mqtt.client.Client
    :type userdata: a dict with at least the following entries:
        - topic: the root topic where we listen all the messages
        - master_topic: the topic used by workers to talk to the master
        - worker_topic: the topic used by master to talk to the workers
        - qos: the QoS for the messenging part
    :type flags: string -- not used --
    :type return_code: integer

    :seealso: http://www.eclipse.org/paho/clients/python/docs/#callbacks
    """
    logger.info(
        'connected to MQTT Broker with status {} for subscribing',
        str(return_code),
    )
    qos = userdata['qos']
    topic = userdata['topic']
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    logger.info(
        'subscribing on topic {} with qos {}',
        topic,
        qos,
    )
    client.subscribe('{}/#'.format(topic), qos)


def on_message(_client, userdata, msg):
    """Use when message is received.

    Function launched when a message is received on MQTT topic.

    :param _client: the client that has received the message
    :param userdata: the userdata defined on the client side
    :param msg: the message object

    :type _client: paho.mqtt.client.Client  -- not used --
    :type userdata: a dict with at least the following entries:
        - topic: the root topic where we listen all the messages
        - subtopics: all the subtopics we're interested in
        - master_topic: the topic used by workers to talk to the master
        - worker_topic: the topic used by master to talk to the workers
        - master: the Master instance which all the messages
    :type msg: paho.mqtt.client.MQTTMessage

    :seealso: http://www.eclipse.org/paho/clients/python/docs/#callbacks
    """
    logger.debug('received a message')
    try:
        json_message = loads(msg.payload)
        logger.debug('message: {}', json_message)
        logger.debug("launching wait for gate result in a thread")
        thread = threading.Thread(target=conditionally_launch_gate, args=(
            json_message,
            userdata,
        ))
        thread.start()
    except JSONDecodeError:
        logger.error("This is not a valid JSON payload")


def _get_config() -> Union[ConfigParser, None]:
    """Read the config file and return the configuration.

    :returns: configparser.ConfigParser object

    :seealso: https://docs.python.org/3/library/configparser.html
    """
    config_file = getenv('CONFIG_FILE')
    config = None
    if config_file:
        logger.info('retrieving {} configuration file', config_file)
        config = ConfigParser()
        config.read(config_file)
    return config


def _parse_config(
    config: Union[ConfigParser, None]
) -> Union[None, Mapping[str, Mapping[str, Union[str, int, Mapping[str, str]]]]]:
    parsed_config = None
    if config:
        logger.debug(config.sections())
        # Configure auth
        auth = None
        mqtt_username = config.get('mqtt', 'username', fallback=None)
        mqtt_password = config.get('mqtt', 'password', fallback=None)
        if mqtt_username:
            auth = {'username': mqtt_username}
            if mqtt_password:
                auth['password'] = mqtt_password

        parsed_config = {
            'gitlab': {
                'external_status_check_id': config.getint(
                    'gitlab',
                    'external_status_check_id',
                ),
                'hostname': config.get('gitlab', 'hostname'),
            },
            'mqtt': {
                'qos': config.getint('mqtt', 'qos', fallback=0),
                'port': config.getint('mqtt', 'port', fallback=1883),
                'keepalive': config.getint('mqtt', 'keepalive', fallback=60),
                'hostname': config.get('mqtt', 'hostname'),
                'auth': auth,
                'subscription_topic': config.get('mqtt', 'subscription_topic'),
                'transport': config.get('mqtt', 'transport', fallback='tcp'),
                'notification_subtopic': config.get(
                    'mqtt',
                    'notification_subtopic',
                ),
            },
        }
    logger.debug("config parsed: {}", parsed_config)
    return parsed_config


def _create_mqtt_client(
    config,
    name,
    on_connect_callback,
    on_message_callback=None,
    userdata=None,
):
    """Configure, create and connect an MQTT client.

    :param config: the config to apply
    :param name: the name of the client
    :param on_connect_callback: the on_connect callback method
    :param on_message_callback: the on_message callback method
    :param userdata: the userdata to add to the client

    :type config:  dict
    :type name: string
    :type on_connect_callback: function name
    :type userdata: dict, default to None
    :type on_message_callback: function name, default to None

    :returns: an MQTT client

    :rtype: paho.mqtt.client.Client
    """
    logger.info('configure and connect MQTT client {}', name)
    mqtt_config = config['mqtt']
    client = mqtt.Client(
        transport=mqtt_config['transport'],
        userdata=userdata,
    )
    client.on_connect = on_connect_callback
    if on_message_callback:
        client.on_message = on_message_callback
    if mqtt_config['transport'] == 'websockets':
        client.ws_set_options(
            path=mqtt_config['mqtt_path'],
            headers=mqtt_config['mqtt_headers'],
        )
    client.enable_logger(logger)
    client.connect(
        mqtt_config['hostname'],
        mqtt_config['port'],
        mqtt_config['keepalive'],
    )
    return client


if __name__ == '__main__':
    start()
