# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
from dataclasses import dataclass
from time import sleep
from typing import Union, Optional

from gitlab.client import Gitlab
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects import Project, ProjectJob, ProjectPipeline, ProjectMergeRequest
from loguru import logger

from gating_launcher.constants import FINISHED_STATUSES, TIMEOUT_STATUS


@dataclass
class GitlabCall:  # pylint: disable=too-many-instance-attributes
    # For now, we'll keep all these attributes
    # needs to be refactored one day
    job: str
    last_commit_id: str
    source_project_id: int
    target_project_id: int
    merge_request_id: int
    private_token: str
    external_status_check_id: int
    host: str = "https://gitlab.com"
    wait_time: int = 10
    max_retries: int = 90
    pipeline_id: int = 0
    target_project: Optional[Project] = None
    merge_request: Optional[ProjectMergeRequest] = None
    check_job: bool = True

    def __post_init__(self) -> None:
        self.gitlab = Gitlab(self.host, private_token=self.private_token)
        logger.debug(
            "{} waiting {}s before retrieving informations",
            self._logger_header(),
            self.wait_time,
        )
        sleep(self.wait_time)
        logger.debug(
            "{} retrieving source project with id {}",
            self._logger_header(),
            self.source_project_id,
        )
        self.source_project = self.gitlab.projects.get(
            self.source_project_id,
            retry_transient_errors=True,
        )
        logger.debug(
            "{} retrieving target project with id {}",
            self._logger_header(),
            self.target_project_id,
        )
        self.target_project = self.gitlab.projects.get(
            self.target_project_id,
            retry_transient_errors=True,
        )
        pipeline = self.get_pipeline()
        if pipeline:
            logger.debug(
                "{} checking freshness of given sha {}",
                self._logger_header(),
                self.last_commit_id,
            )
            self.check_freshness(pipeline)
        else:
            logger.debug(
                "{} can't find the pipeline, aborting",
                self._logger_header(),
            )
            self.check_job = False

    @property
    def source_git_path(self) -> str:
        return self.source_project.http_url_to_repo

    @property
    def target_git_path(self) -> str:
        return self.target_project.http_url_to_repo

    def get_pipeline(self) -> Optional[ProjectPipeline]:
        pipeline = None
        logger.debug(
            "{} Searching merge request {} on target project {} ({})",
            self._logger_header(),
            self.merge_request_id,
            self.target_project.name,
            self.target_project_id,
        )
        self.merge_request = self.target_project.mergerequests.get(
            self.merge_request_id,
            retry_transient_errors=True,
        )
        self.pipeline_id = self.merge_request.head_pipeline['id']
        try:
            logger.debug(
                "{} Searching pipeline on source project {} ({})",
                self._logger_header(),
                self.source_project.name,
                self.source_project_id,
            )
            pipeline = self.source_project.pipelines.get(
                self.pipeline_id,
                retry_transient_errors=True,
            )
            logger.debug(
                "{} Pipeline has been found on source project {} ({})",
                self._logger_header(),
                self.source_project.name,
                self.source_project_id,
            )
        except GitlabGetError:
            logger.warning(
                "{} Pipeline not found in source project",
                self._logger_header(),
            )
        if not pipeline:
            logger.debug(
                "{} Searching pipeline on target project {} ({})",
                self._logger_header(),
                self.target_project.name,
                self.target_project_id,
            )
            try:
                pipeline = self.target_project.pipelines.get(
                    self.pipeline_id,
                    retry_transient_errors=True,
                )
                logger.debug(
                    "{} Pipeline has been found on target project {} ({})",
                    self._logger_header(),
                    self.pipeline_id,
                    self.target_project.name,
                    self.target_project_id,
                )
            except GitlabGetError:
                logger.error(
                    "{} Pipeline not found also in target project",
                    self._logger_header(),
                )
        return pipeline

    def get_job(self) -> Union[ProjectJob, None]:
        pipeline = self.get_pipeline()
        if pipeline:
            self.check_freshness(pipeline)
            jobs = pipeline.jobs.list(retry_transient_errors=True)
            logger.debug(
                "{} {} jobs are present in the pipeline",
                self._logger_header(),
                len(jobs),
            )
            for job in jobs:
                logger.debug(
                    "{} checking job {}",
                    self._logger_header(),
                    job.name,
                )
                if job.name == self.job:
                    return job
        logger.warning(
            "{} Job {} not found",
            self._logger_header(),
            self.job,
        )
        return None

    def wait_for_end(self) -> str:
        retry = 0
        job_id = "Unknown"
        while self.check_job and retry < self.max_retries:
            logger.debug(
                "{} Retry {} out of {}",
                self._logger_header(),
                retry,
                self.max_retries
            )
            job = self.get_job()
            if job:
                job_id = job.id
                logger.debug(
                    "{} Check if job {} has finished, current_status: {}",
                    self._logger_header(),
                    job.id,
                    job.status,
                )
                if job.status in FINISHED_STATUSES:
                    logger.debug(
                        "{} Job {} finished with status: {}",
                        self._logger_header(),
                        job.id,
                        job.status,
                    )
                    return job.status
            sleep(self.wait_time)
            retry += 1
        logger.debug(
            "{} Job {} was too long to finish",
            self._logger_header(),
            job_id,
        )
        return TIMEOUT_STATUS

    def comment(self, message: str) -> None:
        self.merge_request.notes.create({'body': message})

    def _logger_header(self) -> str:
        project = "Not retrieved yet"
        if self.target_project:
            project = self.target_project.name
        logger_header = "project {} - project_id {} - ".format(
            project,
            self.target_project_id,
        )
        logger_header += "merge request {} - pipeline {} -".format(
            self.merge_request_id,
            self.pipeline_id,
        )
        return logger_header

    def check_freshness(self, pipeline: ProjectPipeline) -> None:
        logger.warning(
            "{} Checking if sha {} is the same sha as newest pipeline ({})",
            self._logger_header(),
            self.last_commit_id,
            pipeline.sha,
        )
        if pipeline.sha != self.last_commit_id:
            logger.warning(
                "{} Given pipeline is not done on the latest sha ({}){}",
                self._logger_header(),
                self.last_commit_id,
                ", not launching gate",
            )
            self.check_job = False
