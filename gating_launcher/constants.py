#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

TIMEOUT_STATUS = "timeout"
SUCCESS_STATUS = "success"
FAILED_STATUS = "failed"
CANCELED_STATUS = "canceled"
SKIPPED_STATUS = "skipped"
FINISHED_STATUSES = [
    SUCCESS_STATUS,
    FAILED_STATUS,
    CANCELED_STATUS,
    SKIPPED_STATUS,
]
